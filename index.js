import queue from './task';
import express from 'express';
import Arena from './bull-arena';

const app = express();

const arenaConfig = Arena({
    queues: [
        queue
    ]
},
{ 
    basePath: '/',
    disableListen: true
});

app.get('/', (req, res) => {
    res.send('Остальное что-нибудь!');
});

app.use('/arena', arenaConfig);

app.listen(4000, function () {
    console.log("\x1b[37m", 'Bull-arena listening: http://localhost:4000/arena !');
});